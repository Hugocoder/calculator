import java.util.Scanner;
public class UserInterface
{
    Scanner sc = new Scanner(System.in);
    Calculator calculator = null;
    private int choice;
    private int noOfNumbers;
    private int numbers[];


    public void generateUi()
    {
        System.out.println("<<<<<<<<<<<<<  CALCULATOR  >>>>>>>>>>>>>");
        System.out.println("Which calculator to use ?");
        System.out.println("1. Normal Calculator");
        System.out.println("2. Magic Calculator");
        System.out.println("3. Exit");
        choice = sc.nextInt();
        calculator = CalculatorFactory.calculatorfactory(choice);
        if (choice == 3)
            {
                System.exit(0);
            }
        System.out.println("1.Add\n2.Subtract");
        System.out.println("Enter you option : ");
        choice = sc.nextInt();
        System.out.println("Enter the no of numbers :");
        noOfNumbers = sc.nextInt();
        numbers = new int[noOfNumbers];
        System.out.println("Enter the numbers : ");
        for(int loopValue=0;loopValue<noOfNumbers;loopValue++)
        {
            numbers[loopValue] = sc.nextInt();
        }

        switch(choice)
        {
            case 1:
                calculator.add(numbers);
                System.out.println("RESULT : "+ calculator.getResult());
                break;
            case 2:
                calculator.sub(numbers);
                System.out.println("RESULT : "+ calculator.getResult());
                break;
            default :
                System.out.println("Invalid Choice!");

        }
    }
}

public abstract class Calculator
{
    protected int result;
    protected  Chipset chipSet = null;

    public abstract int add(int numbers[]);
    public abstract int sub(int numbers[]);

    public Calculator()
    {
    chipSet = new Chipset();
    }
    public int getResult()
    {
        return result;
    }
}
